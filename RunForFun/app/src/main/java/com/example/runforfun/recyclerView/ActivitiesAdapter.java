package com.example.runforfun.recyclerView;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.example.runforfun.database.AppRepository;
import com.example.runforfun.R;
import com.example.runforfun.database.RunItem;
import com.example.runforfun.utilities.Utility;
import java.util.ArrayList;
import java.util.List;

public class ActivitiesAdapter extends RecyclerView.Adapter<ActivitiesViewHolder>{

    private List<RunItem> activities = new ArrayList<>();
    private OnItemListener listener;
    private Activity activity;

    public ActivitiesAdapter(OnItemListener listener, Activity activity) {
        this.listener = listener;
        this.activity = activity;
    }

    /**
     *
     * Called when RecyclerView needs a new RecyclerView.ViewHolder of the given type to represent an item.
     *
     * @param parent ViewGroup into which the new View will be added after it is bound to an adapter position.
     * @param viewType view type of the new View.
     * @return A new ViewHolder that holds a View of the given view type.
     */
    @NonNull
    @Override
    public ActivitiesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.runs_list_item,
                parent, false);
        return new ActivitiesViewHolder(layoutView, listener) {
            @Override
            public void deleteItem(int pos) {
                RunItem runItem = activities.get(pos);
                int id = runItem.getRun_item_id();
                AppRepository appRepository = new AppRepository(activity.getApplication());
                appRepository.deleteDetailItems(id);
                appRepository.deleteRunItem(id);
            }
        };
    }

    /**
     * Called by RecyclerView to display the data at the specified position.
     * This method should update the contents of the RecyclerView.ViewHolder.itemView to reflect
     * the item at the given position.
     *
     * @param holder ViewHolder which should be updated to represent the contents of the item at
     *               the given position in the data set.
     * @param position position of the item within the adapter's data set.
     */
    @Override
    public void onBindViewHolder(@NonNull ActivitiesViewHolder holder, int position) {
        final RunItem runItem = activities.get(position);
        holder.dateTextView.setText(runItem.getDate());
        holder.timeTextView.setText(Utility.getTimeFormat(runItem.getTime()));
        holder.kilometersTextView.setText(holder.itemView.getContext().getString(R.string.km, Double.toString(runItem.getDistance())));
    }

    @Override
    public int getItemCount() {
        return activities.size();
    }


    public RunItem getRunItem(int position) {
        return activities.get(position);
    }

    /**
     * Method called when a new item is added
     * @param newData the new list of items
     */
    public void setData(List<RunItem> newData) {
        this.activities.clear();
        this.activities.addAll(newData);
        notifyDataSetChanged();
    }

}
