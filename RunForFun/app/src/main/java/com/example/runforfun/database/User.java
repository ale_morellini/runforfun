package com.example.runforfun.database;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

/**
 * Class which represents the main user
 */
@Entity
public class User {
    @PrimaryKey
    @ColumnInfo(name = "id")
    public int  id;
    @ColumnInfo(name = "first_name")
    public String firstName;
    @ColumnInfo(name = "last_name")
    public String lastName;
    @ColumnInfo(name = "height")
    public int height;
    @ColumnInfo(name = "weight")
    public int weight;
    @ColumnInfo(name = "bmi")
    public int bmi;
    @ColumnInfo(name = "birthdate")
    public String birthdate;
    @ColumnInfo(name = "sex")
    public String sex;


    public User(int id, String firstName, String lastName, int height, int weight, int bmi, String birthdate, String sex) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.height = height;
        this.weight = weight;
        this.bmi = bmi;
        this.birthdate = birthdate;
        this.sex = sex;
    }

    public int getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public int getHeight() {
        return height;
    }

    public int getWeight() {
        return weight;
    }

    public int getBmi() {
        return bmi;
    }

    public String getBirthDate() {
        return birthdate;
    }

    public String getSex() {
        return sex;
    }
}
