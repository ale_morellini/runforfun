package com.example.runforfun.chart;

import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.formatter.ValueFormatter;

import java.util.Locale;

/**
 * Formatter class for X axis of Detail items chart
 */
public class Xformatter extends ValueFormatter {

    private String symbol = "";

    public Xformatter(String symbol){
        this.symbol = symbol;
    }

    @Override
    public String getAxisLabel(float value, AxisBase axis) {

        return String.format(Locale.ITALY,"%.2f", value) + " " + symbol;
    }

}
