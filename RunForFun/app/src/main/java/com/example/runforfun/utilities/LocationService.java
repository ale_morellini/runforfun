package com.example.runforfun.utilities;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;

import com.example.runforfun.MainActivity;
import com.example.runforfun.R;
import com.example.runforfun.database.RunDetailItem;
import com.example.runforfun.RunDetailItemSingleton;

import java.math.BigDecimal;
import java.util.Objects;


/**
 * Class for handler location changes during run. According to position updates it can also
 * send a request to NotificationService in order to send notification to user.
 */
public class LocationService extends Service implements LocationListener {

    public static final String CHANNEL_ID = "ForegroundServiceChannel";

    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 20;
    private static final long MIN_TIME_BW_UPDATES = 2 * 1000;
    public static final String ACTION_LOCATION = "com.example.runforfun.action.location";
    private RunDetailItemSingleton runDetailItemSingleton;
    private double distance;
    private int calories;
    private RunUtilities runUtilities;
    private long timeStart;
    private int distanceNotificationSetting;
    private long pauseTimeNotificationSetting;
    private boolean pauseNotificationActive;
    private boolean distanceNotificationActive;
    private Double prevDistance = 0.0;
    private int prevPause = 0;
    private HandlerThread handlerThread;

    @Override
    public void onCreate() {

        super.onCreate();
        handlerThread = new HandlerThread("LocationThread");
        handlerThread.start();
        Looper looper = handlerThread.getLooper();
        Handler handler = new Handler(looper);
        handler.post(() -> {
            runDetailItemSingleton = RunDetailItemSingleton.getInstance();
            runUtilities = new RunUtilities();
            timeStart = System.currentTimeMillis();
            loadPreferences();
        });
    }


    /**
     * Function which set gps interval and priority. request updated to FusedLocationProviderClient
     */
    public void requestUpdates(){

        try {
            LocationManager locManager = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
            if(locManager != null) {
                locManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, MIN_TIME_BW_UPDATES,
                        MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
            }
        }catch (SecurityException e){
            e.printStackTrace();
        }
    }


    @Override
    public void onLocationChanged(Location location) {
        double lat1 = location.getLatitude();
        double lon1 = location.getLongitude();
        double speed = location.getSpeed();
        runDetailItemSingleton.addRunItem(new RunDetailItem(lat1, lon1, speed));

        //performing some calculus
        calculatePath();
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @Override
    public void onDestroy() {
        stopRequest();
        handlerThread.quit();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    /**
     * Method used to stop requesting updates from locationManager
     */
    private void stopRequest(){
        try {
            LocationManager locManager = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
            if(locManager != null) {
                locManager.removeUpdates(this);
            }
        }catch (SecurityException e){
            e.printStackTrace();
        }
    }

    /**
     * This function allows to send a notification about distance.
     * Sends an intent to "NotificationBroadcastReceiver" that "builds" in concrete
     * the notification. Notifications are sent when a certain amount of
     * km have been done. This "amount" is an "user preference".
     * @param actualDistance actual distance.
     */
    private void sendRunNotification(Double actualDistance){
        if(distanceNotificationActive) {

            if (actualDistance - prevDistance >= distanceNotificationSetting) {
                prevDistance = actualDistance;

                Intent notify = new Intent();
                notify.setAction(NotificationBroadcastReceiver.ACTION_NOTIFICATION);
                notify.putExtra("distance", String.valueOf(actualDistance));
                sendBroadcast(notify);
            }
        }
    }

    /**
     * This function allows to send a pause notification.
     * @param time represents how much time user has been running.
     */
    private void sendPauseNotification(long time) {
        if (pauseNotificationActive) {
            if ((time/60000) - prevPause >= (pauseTimeNotificationSetting)) {
                prevPause += pauseTimeNotificationSetting;

                Intent notify = new Intent();
                notify.setAction(NotificationBroadcastReceiver.ACTION_NOTIFICATION);
                notify.putExtra("time", String.valueOf(prevPause));
                sendBroadcast(notify);
            }
        }
    }

    /**
     * Simple function to load user preferences. Allows to know if notifications has to be delivered.
     */
    private void loadPreferences(){
        SharedPreferences sharedPreferences = getSharedPreferences(Utility.KEY_NOTIFICATION_DIST_TRIGGER,Context.MODE_PRIVATE);
        if(sharedPreferences!=null){
            distanceNotificationSetting = sharedPreferences.getInt(Utility.KEY_NOTIFICATION_DIST_TRIGGER,Utility.MINIMUM_DISTANCE_TRIGGER);
        }

        sharedPreferences = getSharedPreferences(Utility.KEY_NOTIFICATION_PAUSE_TRIGGER,Context.MODE_PRIVATE);
        if(sharedPreferences!=null){
            pauseTimeNotificationSetting = sharedPreferences.getLong(Utility.KEY_NOTIFICATION_PAUSE_TRIGGER,Utility.MINIMUM_PAUSE_TIME_TRIGGER);
        }

        sharedPreferences = getSharedPreferences(Utility.KEY_NOTIFICATION_PAUSE_ACTIVE,Context.MODE_PRIVATE);
        if(sharedPreferences!=null){
            pauseNotificationActive = sharedPreferences.getBoolean(Utility.KEY_NOTIFICATION_PAUSE_ACTIVE, false);
        }

        sharedPreferences = getSharedPreferences(Utility.KEY_NOTIFICATION_DIST_ACTIVE,Context.MODE_PRIVATE);
        if(sharedPreferences!=null){
            distanceNotificationActive = sharedPreferences.getBoolean(Utility.KEY_NOTIFICATION_DIST_ACTIVE, false);
        }
    }

    /**
     * Simple function to calculate distance, calories and speed according to new position infos.
     */
    private void calculatePath(){
        if(runDetailItemSingleton.getRunItems().size() > 1){
            double lat2 = runDetailItemSingleton.getSingleItem(runDetailItemSingleton.getRunItems().size() -1).getLatitude();
            double lon2 = runDetailItemSingleton.getSingleItem(runDetailItemSingleton.getRunItems().size() -1).getLongitude();
            double lat1 = runDetailItemSingleton.getSingleItem(runDetailItemSingleton.getRunItems().size() -2).getLatitude();
            double lon1 = runDetailItemSingleton.getSingleItem(runDetailItemSingleton.getRunItems().size() -2).getLongitude();
            distance += runUtilities.distanceBetweenTwoPoint(lat1, lon1, +lat2, lon2);
        }
        double speed = runDetailItemSingleton.getSingleItem(runDetailItemSingleton.getRunItems().size() -1).getSpeed();
        calories += runUtilities.calculateCalories(speed);


        //updating RunFragment with calculated values
        Double realDistance = new BigDecimal(distance).setScale(2 , BigDecimal.ROUND_UP).doubleValue();
        Intent sendRunFragmentInfo = new Intent();
        sendRunFragmentInfo.setAction(ACTION_LOCATION);
        sendRunFragmentInfo.putExtra("calories",calories);
        sendRunFragmentInfo.putExtra("distance",realDistance);
        sendRunFragmentInfo.putExtra("speed",speed);
        getApplicationContext().sendBroadcast(sendRunFragmentInfo);

        long durationValue = System.currentTimeMillis() - timeStart;
        //sending pause notification
        sendPauseNotification(durationValue);

        //sending distance notification to user
        sendRunNotification(realDistance);
    }


    public int onStartCommand(Intent intent, int flags, int startId) {

        //checking if service should start or stop
        if (Objects.equals(intent.getAction(), Utility.STOP_SERVICE)) {
            stopForeground(true);
            stopSelf();
        }
        else {
            String input = intent.getStringExtra("inputExtra");
            createNotificationChannel();
            Intent notificationIntent = new Intent(this, MainActivity.class);
            PendingIntent pendingIntent = PendingIntent.getActivity(this,
                    0, notificationIntent, 0);
            Notification notification = new NotificationCompat.Builder(this, CHANNEL_ID)
                    .setContentTitle("RunForFun tracker")
                    .setContentText(input)
                    .setSmallIcon(R.drawable.icon)
                    .setContentIntent(pendingIntent)
                    .build();
            startForeground(10, notification);
            requestUpdates();
        }
        return START_NOT_STICKY;
    }

    /**
     * This method allows to create a channel in which send notification of
     * background location service.
     */
    private void createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel serviceChannel = new NotificationChannel(
                    CHANNEL_ID,
                    "Foreground Service Channel",
                    NotificationManager.IMPORTANCE_DEFAULT
            );
            NotificationManager manager = getSystemService(NotificationManager.class);
            if (manager != null) {
                manager.createNotificationChannel(serviceChannel);
            }
        }
    }

}
