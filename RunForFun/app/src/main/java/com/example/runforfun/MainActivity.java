package com.example.runforfun;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.example.runforfun.utilities.Utility;

public class MainActivity extends AppCompatActivity {

    private static final String HOME_FRAGMENT_TAG = "homeFragment";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (savedInstanceState == null) {
            Utility.insertFragment(this, R.id.fragment_container, new HomeFragment(), HOME_FRAGMENT_TAG);
        }
    }

}
