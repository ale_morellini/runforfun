package com.example.runforfun;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import com.example.runforfun.utilities.Utility;

public class RecapActivity extends AppCompatActivity {

    private static final String RECAP_FRAGMENT_TAG = "recapFragment";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recap);

        if (savedInstanceState == null) {
            Utility.insertFragment(this, R.id.recap_container, new RecapFragment(), RECAP_FRAGMENT_TAG);
        }
    }
}

