package com.example.runforfun;

import com.example.runforfun.database.RunDetailItem;

import java.util.ArrayList;
import java.util.List;

/**
 * This represents a temporary storage used to collect gps positions in a run.
 */
public class RunDetailItemSingleton {

    private List<RunDetailItem> items = new ArrayList<>();

    private static final RunDetailItemSingleton ourInstance = new RunDetailItemSingleton();

    /**
     * Getter for run detail items singleton.
     * @return RunDetailItemSingleton instance of singleton
     */
    public static RunDetailItemSingleton getInstance() {return ourInstance; }

    private RunDetailItemSingleton(){

    }

    /**
     * Function called when a run item is added
     * @param item RunDetailItem
     */
    public void addRunItem(RunDetailItem item) { items.add(item); }

    /**
     * getter for the list of RunDetailItem
     * @return list of Run items
     */
    public List<RunDetailItem> getRunItems() { return items; }

    /**
     * function that clear all item in the singleton
     */
    void clearItems() { items.clear(); }

    /**
     * Getter for a specific item in the index position from the singleton
     * @param index position in the singleton
     * @return RunDetailItem item
     */
    public RunDetailItem getSingleItem(int index) {return items.get(index); }

}
