package com.example.runforfun;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.widget.DatePicker;

import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;

import com.google.android.material.textfield.TextInputEditText;

import java.util.Calendar;
import java.util.Date;

/**
 * Class to build a DatePicker. Is used to set user Birthdate.
 */
public class DatePickerFragment extends DialogFragment
        implements DatePickerDialog.OnDateSetListener {

    private TextInputEditText birth_date;
    private Activity activity;

    DatePickerFragment(Activity activity, TextInputEditText birth_date) {
        this.activity = activity;
        this.birth_date = birth_date;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the current date as the default date in the picker
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        // Create a new instance of DatePickerDialog and return it
        DatePickerDialog datePickerDialog = new DatePickerDialog(activity, this, year, month, day);
        DatePicker datePicker = datePickerDialog.getDatePicker();

        //before returning the dialog let's set up some specs
        long today = new Date().getTime();
        long maxDate = today - (long) 14*365*3600*24*1000; // user must have at least 14 years old
        long minDate = today - (long) 100*365*3600*24*1000; // user max age is 100 year. Too much to run isn't it?

        datePicker.setMaxDate(maxDate);
        datePicker.setMinDate(minDate);

        return datePickerDialog;
    }

    public void onDateSet(DatePicker view, int year, int month, int day) {
        birth_date.setText(getString(R.string.birth_date_value,day,month,year));
    }
}