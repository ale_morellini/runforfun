package com.example.runforfun.recyclerView;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.runforfun.R;

public abstract class ActivitiesViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    ImageView runImage;
    TextView kilometersTextView;
    TextView timeTextView;
    TextView dateTextView;
    private OnItemListener itemListener;

    ActivitiesViewHolder(@NonNull View itemView, OnItemListener lister) {
        super(itemView);
        runImage = itemView.findViewById(R.id.runItemImageView);
        kilometersTextView = itemView.findViewById(R.id.runItemKilometersTextView);
        timeTextView = itemView.findViewById(R.id.runItemTimeTextView);
        dateTextView = itemView.findViewById(R.id.runItemDateTextView);
        itemListener = lister;

        itemView.findViewById(R.id.trashItemImageView).setOnClickListener(v -> new Thread(() -> deleteItem(getAdapterPosition())).start());

        itemView.setOnClickListener(this);
    }

    /**
     * implementation of method in View.OnclickListener
     * @param v the view
     */
    @Override
    public void onClick(View v) {
        itemListener.onItemClick(getAdapterPosition());
    }

    public abstract void deleteItem(int pos);
}