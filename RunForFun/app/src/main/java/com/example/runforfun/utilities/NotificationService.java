package com.example.runforfun.utilities;

import android.app.Service;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import androidx.annotation.Nullable;

/**
 * This class is a service that runs in background and allows user to receive notification.
 */
public class NotificationService extends Service {
    private NotificationBroadcastReceiver notificationBroadcastReceiver;
    private HandlerThread handlerThread;


    @Override
    public void onCreate() {
        super.onCreate();
        handlerThread = new HandlerThread("NotificationThread");
        handlerThread.start();
        Looper looper = handlerThread.getLooper();
        Handler handler = new Handler(looper);
        handler.post(() -> {

            //below receiver has the logic of notification, it builds notifications too
            notificationBroadcastReceiver = new NotificationBroadcastReceiver();
            IntentFilter filter = new IntentFilter();
            filter.addAction(NotificationBroadcastReceiver.ACTION_NOTIFICATION);
            registerReceiver(notificationBroadcastReceiver,filter);
        });

    }

    @Override
    public void onDestroy() {
        unregisterReceiver(notificationBroadcastReceiver);
        handlerThread.quit();
        super.onDestroy();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
