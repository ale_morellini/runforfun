package com.example.runforfun;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.example.runforfun.utilities.Utility;

/**
 * Activity for list of user runs called "activities"
 */
public class ActivitiesActivity extends AppCompatActivity {

    private static final String ACTIVITIES_FRAGMENT_TAG = "ActivitiesFragment";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_activities);

        if (savedInstanceState == null) {
            Utility.insertFragment(this, R.id.fragment_container_activities, new ActivitiesFragment(), ACTIVITIES_FRAGMENT_TAG);
        }
    }
}
