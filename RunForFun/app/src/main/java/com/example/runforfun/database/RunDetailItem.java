package com.example.runforfun.database;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

/**
 * Class which represents a detailed piece of a single run:
 * For example, in a run that lasts 1 hour we could have 60 detailed items.
 */
@Entity
public class RunDetailItem {

    @PrimaryKey(autoGenerate = true)
    private int run_detail_id;
    @ColumnInfo(name = "runId")
    private int runId;
    @ColumnInfo(name = "latitude")
    private double latitude;
    @ColumnInfo(name = "longitude")
    private double longitude;
    @ColumnInfo(name = "speed")
    private double speed;


    public RunDetailItem(double latitude, double longitude, double speed) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.speed = speed;
    }

    public int getRun_detail_id() {
        return run_detail_id;
    }

    public void setRun_detail_id(int run_detail_id) {
        this.run_detail_id = run_detail_id;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public double getSpeed() {
        return speed;
    }

    public int getRunId() {
        return runId;
    }

    public void setRunId(int runId) {
        this.runId = runId;
    }

}

