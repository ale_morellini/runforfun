package com.example.runforfun;

import android.os.Handler;

/**
 * This class is used to check if a thread has completed. Then offers an abstract method to do something on finish.
 */
abstract class DataBaseWorkerChecker {

    private int millis;
    private Thread thread;

    DataBaseWorkerChecker(int millis, Thread thread){
        this.millis = millis;
        this.thread = thread;
        checkDataBaseWorkerEnd();
    }

    /**
     * This is a recursive function. Checks if thread passed by argument
     * has finished. If not just iterates. Checking process repeats every "millis".
     */
    private void checkDataBaseWorkerEnd( ){

        Handler handler = new Handler();
        handler.postDelayed(() -> {
            if(thread.getState() == Thread.State.TERMINATED){
                action();
            }else {
                checkDataBaseWorkerEnd();
            }
        }, millis);
    }

    /**
     * Action to do when thread has finished.
     */
    abstract void action();

}
