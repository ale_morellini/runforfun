package com.example.runforfun.database;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

/**
 * Class which represents a single run
 */
@Entity
public class RunItem {

    @PrimaryKey(autoGenerate = true)
    private int run_item_id;
    @ColumnInfo(name = "time")
    private long time;
    @ColumnInfo(name = "distance")
    private double distance;
    @ColumnInfo(name = "avg_speed")
    private double avg_speed;
    @ColumnInfo(name = "rythm")
    private String rythm;
    @ColumnInfo(name = "calories")
    private int calories;
    @ColumnInfo(name = "date")
    private String date;


    public RunItem(long time, double distance, double avg_speed, String rythm, int calories, String date) {
        this.time = time;
        this.distance = distance;
        this.avg_speed = avg_speed;
        this.rythm = rythm;
        this.calories = calories;
        this.date = date;
    }

    public int getRun_item_id() {
        return run_item_id;
    }

    public void setRun_item_id(int run_item_id) {
        this.run_item_id = run_item_id;
    }

    public long getTime() {
        return time;
    }

    public double getDistance() { return distance; }

    public double getAvg_speed() {
        return avg_speed;
    }

    public String getRythm() {
        return rythm;
    }

    public int getCalories() {
        return calories;
    }

    public String getDate() {
        return date;
    }
}

