package com.example.runforfun;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.example.runforfun.chart.Xformatter;
import com.example.runforfun.database.AppRepository;
import com.example.runforfun.database.RunDetailItem;
import com.example.runforfun.database.RunItem;
import com.example.runforfun.database.RunItemComplete;
import com.example.runforfun.utilities.Utility;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.mapbox.geojson.Feature;
import com.mapbox.geojson.FeatureCollection;
import com.mapbox.geojson.LineString;
import com.mapbox.geojson.Point;
import com.mapbox.mapboxsdk.Mapbox;
import com.mapbox.mapboxsdk.camera.CameraPosition;
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;
import com.mapbox.mapboxsdk.maps.Style;
import com.mapbox.mapboxsdk.style.layers.LineLayer;
import com.mapbox.mapboxsdk.style.layers.Property;
import com.mapbox.mapboxsdk.style.sources.GeoJsonOptions;
import com.mapbox.mapboxsdk.style.sources.GeoJsonSource;

import java.util.ArrayList;
import java.util.List;

import static com.example.runforfun.ActivitiesFragment.CALLER_ITEM_ID;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.lineCap;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.lineColor;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.lineJoin;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.lineOpacity;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.lineWidth;

public class ActivitiesDetailFragment extends Fragment implements  BottomNavigationView.OnNavigationItemSelectedListener, OnMapReadyCallback, BottomNavigationView.OnNavigationItemReselectedListener {

    private AppRepository appRepository;
    private int callingItemId = -1;
    private TextView time;
    private TextView distance;
    private TextView avg_speed;
    private TextView avg_rhythm;
    private TextView calories;
    private List<RunDetailItem> runDetailItems;
    private List<Point> routeCoordinates;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Mapbox Access token
        Activity activity = getActivity();
        if(activity!=null) {
            Mapbox.getInstance(activity.getApplicationContext(), getString(R.string.mapbox_access_token));
        }
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            callingItemId = bundle.getInt(CALLER_ITEM_ID, -1);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        return inflater.inflate(R.layout.fragment_activities_detail, container,false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        final AppCompatActivity activity = (AppCompatActivity) getActivity();
        if(activity!=null) {

            BottomNavigationView bottomNavigationView = activity.findViewById(R.id.bottom_nav_bar);
            bottomNavigationView.setOnNavigationItemSelectedListener(this);
            bottomNavigationView.setSelectedItemId(R.id.attivita);
            bottomNavigationView.setOnNavigationItemReselectedListener(this);

            LineChart chart = activity.findViewById(R.id.speed_chart_view);
            appRepository = new AppRepository(activity.getApplication());

            time = activity.findViewById(R.id.timeDetailValueTextView);
            distance = activity.findViewById(R.id.distanceDetailValueTextView);
            avg_speed = activity.findViewById(R.id.speedDetailValueTextView);
            avg_rhythm = activity.findViewById(R.id.rythmDetailValueTextView);
            calories = activity.findViewById(R.id.caloriesDetailValueTextView);


            Thread chartThread;
            chartThread = new Thread(() -> {

                //in order to show any chart info we should first get a complete run item
                RunItemComplete runItemComplete = appRepository.getCompleteRunById(callingItemId);
                RunItem runItem = runItemComplete.runItem;

                time.setText(Utility.getTimeFormat(runItem.getTime()));
                distance.setText(getString(R.string.km, String.valueOf(runItem.getDistance())));
                avg_speed.setText(getString(R.string.km_h, String.valueOf(runItem.getAvg_speed())));
                avg_rhythm.setText(getString(R.string.min_km, String.valueOf(runItem.getRythm())));
                calories.setText(String.valueOf(runItem.getCalories()));

                double totalDistance = runItem.getDistance();

                //here are stored all positions taken by GPS
                runDetailItems = new ArrayList<>(runItemComplete.runDetails);

                //collecting entries to use in graph
                List<Entry> entries = new ArrayList<>();
                float step = (float) (totalDistance/runDetailItems.size());
                for (RunDetailItem data : runDetailItems) {
                    entries.add(new Entry(step*(runDetailItems.indexOf(data)+1),(float) data.getSpeed()));

                }

                LineData lineData = new LineData(setGraphLayout(chart,entries));
                chart.setData(lineData);
                chart.invalidate(); // refresh

            });

            chartThread.start();
            MapView mapView = activity.findViewById(R.id.mapView);
            mapView.onCreate(savedInstanceState);
            mapView.getMapAsync(this);

        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        AppCompatActivity activity = (AppCompatActivity) getActivity();
        if(activity!=null){
            switch (item.getItemId()) {
                case R.id.profilo:
                    Utility.launchActivity(activity, UserActivity.class);
                    return true;
                case R.id.corsa:
                    Utility.launchActivity(activity, MainActivity.class);
                case R.id.attivita:
                    return true;
                default:
                    return false;
            }
        }
        return false;
    }

    @Override
    public void onMapReady(@NonNull MapboxMap mapboxMap) {
        CameraPosition position = new CameraPosition.Builder()
                .target(new LatLng(runDetailItems.get(0).getLatitude(), runDetailItems.get(0).getLongitude()))
                .zoom(14)
                .tilt(20)
                .build();
        mapboxMap.animateCamera(CameraUpdateFactory.newCameraPosition(position), 500);
        mapboxMap.setStyle(Style.LIGHT, style -> {
            initCoordinates();

            LineString lineString = LineString.fromLngLats(routeCoordinates);

            FeatureCollection featureCollection = FeatureCollection.fromFeature(Feature.fromGeometry(lineString));

            style.addSource(new GeoJsonSource("line-source", featureCollection,
                    new GeoJsonOptions().withLineMetrics(true)));

            style.addLayer(new LineLayer("linelayer", "line-source").withProperties(
                    lineCap(Property.LINE_CAP_ROUND),
                    lineJoin(Property.LINE_JOIN_ROUND),
                    lineOpacity(.7f),
                    lineWidth(7f),
                    lineColor(Color.parseColor("#3bb2d0"))));
        });
    }

    /**
     * Simple coordinate initialization
     */
    private void initCoordinates() {
        routeCoordinates = new ArrayList<>();
        for (RunDetailItem data : runDetailItems) {
            routeCoordinates.add(Point.fromLngLat(data.getLongitude(), data.getLatitude()));
        }
    }

    @Override
    public void onNavigationItemReselected(@NonNull MenuItem item) {
        AppCompatActivity activity = (AppCompatActivity) getActivity();
        if(activity!=null) {
            if (item.getItemId() == R.id.attivita) {
                Utility.replaceFragment(activity, R.id.fragment_container_activities, new ActivitiesFragment(), ActivitiesFragment.class.toString());
            }
        }
    }

    /**
     * Simple method to style a Line chart.
     * @param chart chart to be styled.
     * @param entries entries of chart. Styling modifies them too
     * @return LineDataSet which is styled entities
     */
    private LineDataSet setGraphLayout(LineChart chart, List<Entry> entries ){
        YAxis left = chart.getAxisLeft();
        left.setDrawLabels(true);
        left.setDrawAxisLine(true);
        left.setDrawGridLines(false);
        left.setDrawZeroLine(true);
        chart.getAxisRight().setEnabled(false);

        XAxis xAxis = chart.getXAxis();
        xAxis.setValueFormatter(new Xformatter("Km"));

        LineDataSet dataSet = new LineDataSet(entries, "Velocità Km/h"); // add entries to dataset
        dataSet.setMode(LineDataSet.Mode.CUBIC_BEZIER); //smooth draw
        dataSet.setDrawValues(false);
        dataSet.setLineWidth(5);
        dataSet.setDrawCircleHole(false);
        dataSet.setDrawCircles(false);
        chart.setDrawBorders(true);

        //no description
        Description description = chart.getDescription();
        description.setEnabled(false);
        return dataSet;
    }
}
