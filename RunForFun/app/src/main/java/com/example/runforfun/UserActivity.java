package com.example.runforfun;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.runforfun.utilities.Utility;

import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;

public class UserActivity extends AppCompatActivity {

    private Uri currentPhotoUri;
    private Bitmap imageBitmap;
    private SharedPreferences sharedPreferences;

    private static final String USER_PROFILE_TAG = "UserProfileFragment";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);

        sharedPreferences = getSharedPreferences(Utility.KEY_PROFILE_IMAGE,Context.MODE_PRIVATE);
        if(sharedPreferences != null){
            currentPhotoUri = Uri.parse(sharedPreferences.getString(Utility.KEY_PROFILE_IMAGE, ""));
            if(!currentPhotoUri.toString().equals("")){
                imageBitmap = Utility.getImageBitmap(this, currentPhotoUri);
            }
        }

        if (savedInstanceState == null) {
            //check if there isn't any user so it's first time app's running
            sharedPreferences = getSharedPreferences(Utility.KEY_FIRST_USE, Context.MODE_PRIVATE);

            if(sharedPreferences.getBoolean(Utility.KEY_FIRST_USE,true)){
                //user need's to create a new profile
                Utility.insertFragment(this, R.id.fragment_container_user, new UserProfileEditFragment(), UserProfileEditFragment.EDIT_PROFILE_TAG);

            }else {
                //in this other case just loads user page
                sharedPreferences = getSharedPreferences(Utility.KEY_PROFILE_IMAGE, Context.MODE_PRIVATE);
                imageBitmap = Utility.getImageBitmap(this, Uri.parse(sharedPreferences.getString(Utility.KEY_PROFILE_IMAGE, "")));
                Utility.insertFragment(this, R.id.fragment_container_user, new UserProfileFragment(), USER_PROFILE_TAG);
            }
        }
    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        //save the bitmap and the uri of the image taken
        outState.putParcelable("image", imageBitmap);
        outState.putParcelable("currentPhotoUri", currentPhotoUri);
        super.onSaveInstanceState(outState);
    }

    /**
     * Called after the picture is taken
     * @param requestCode requestcode od the intent
     * @param resultCode result of the intent
     * @param data data of the intent (picture)
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Utility.REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            if (extras != null) {
                imageBitmap = (Bitmap) extras.get("data");
                try {
                    if (imageBitmap != null) {
                        //method to save the image in the gallery of the device
                        saveImage(imageBitmap);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            // Load a specific media item, and show it in the ImageView
            Bitmap bitmap = Utility.getImageBitmap(this, currentPhotoUri);
            setUserPicture(currentPhotoUri.toString());
            if (bitmap != null){
                ImageView imageView = findViewById(R.id.profile_image_edit);
                imageView.setImageBitmap(bitmap);
            }
        }
    }

    /**
     * Method called to save the image taken as a file in the gallery
     * @param bitmap the image taken
     * @throws IOException if there are some issue with the creation of the image file
     */
    private void saveImage(Bitmap bitmap) throws IOException {
        // Create an image file name
        String timeStamp =
                new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.ITALY).format(new Date());
        String name = "JPEG_" + timeStamp + "_.png";


            ContentResolver resolver = getContentResolver();
            ContentValues contentValues = new ContentValues();
            contentValues.put(MediaStore.MediaColumns.DISPLAY_NAME, name + ".jpg");
            contentValues.put(MediaStore.MediaColumns.MIME_TYPE, "image/jpg");
            Uri imageUri = resolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, contentValues);
            currentPhotoUri = imageUri;
            OutputStream fos = resolver.openOutputStream(Objects.requireNonNull(imageUri));

            //for the jpeg quality, it goes from 0 to 100
            //for the png one, the quality is ignored
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
            if (fos != null) {
                fos.close();
            }
        }

    /**
     * Just sets user picture by saving it on a sharedPref v
     * @param path actually we save the path an not the bitmap.
     */
    private void setUserPicture(String path){
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(Utility.KEY_PROFILE_IMAGE, path);
        editor.apply();
    }

    /**
     * Simple getter for imageBitmap
     * @return bitmap
     */
    public Bitmap getImageBitmap() {
        return imageBitmap;
    }

}
