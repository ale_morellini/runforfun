package com.example.runforfun.utilities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;
import android.content.ContentResolver;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.Settings;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;


import com.example.runforfun.BuildConfig;

import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;
import java.util.TimeZone;

/**
 * Generic utility class.
 */
public class Utility {

    public static final String STOP_SERVICE = "stop";
    public static final int REQUEST_IMAGE_CAPTURE = 2;
    public static final String KEY_FIRST_USE = BuildConfig.APPLICATION_ID + ".SHARED_PREF";
    public static final String KEY_PROFILE_IMAGE = BuildConfig.APPLICATION_ID + ".SHARED_PREF_IMAGE";
    public static final String KEY_NOTIFICATION_DIST_TRIGGER = BuildConfig.APPLICATION_ID + ".SHARED_PREF_DIST";
    public static final String KEY_NOTIFICATION_PAUSE_TRIGGER = BuildConfig.APPLICATION_ID + ".SHARED_PREF_PAUSE";
    public static final String KEY_NOTIFICATION_DIST_ACTIVE = BuildConfig.APPLICATION_ID + ".SHARED_PREF_DIST_ACTIVE";
    public static final String KEY_NOTIFICATION_PAUSE_ACTIVE = BuildConfig.APPLICATION_ID + ".SHARED_PREF_PAUSE_ACTIVE";
    public static final String KEY_ACTIVITY_ROTATION = BuildConfig.APPLICATION_ID + ".SHARED_PREF_ROTATION";


    public static final int USER_ID = 1;
    public static final int MINIMUM_PAUSE_TIME_TRIGGER = 1;
    public static final int MINIMUM_DISTANCE_TRIGGER = 1;


    /**
     * Add a fragment to a specific container.
     *
     * @param activity The activity from which retrieve fragment manager.
     * @param containerId Fragment container's id.
     * @param fragment The fragment to be added.  This fragment must not already
     * be added to the activity.
     * @param tag Optional tag name for the fragment.
     *
     */
    public static void insertFragment(AppCompatActivity activity, int containerId, Fragment fragment, String tag) {
        activity.getSupportFragmentManager()
                .beginTransaction()
                .add(containerId, fragment, tag)
                .commit();
    }

    /**
     * Static method to launch an activity trough an explicit intent.
     *
     * @param activity  Context activity (caller).
     * @param newActivity   Class of new activity to be launched.
     */
    public static void launchActivity(AppCompatActivity activity, Class newActivity){
        Intent intent = new Intent(activity.getApplicationContext(), newActivity);
        activity.startActivity(intent);
    }

    /**
     *  Replace a fragment into a specific container.
     *
     * @param activity The activity from which retrieve fragment manager.
     * @param containerId Fragment container's id.
     * @param fragment New fragment.
     * @param tag Optional tag name for the fragment.
     */
    public static void replaceFragment(AppCompatActivity activity, int containerId, Fragment fragment, String tag) {
        activity.getSupportFragmentManager()
                .beginTransaction()
                .replace(containerId, fragment,tag)
                .commit();
    }


    /**
     * This function allows to get an image bitmap from a specified URi
     * @param activity  used to get application.
     * @param currentPhotoUri   bitmap uri.
     * @return Bitmap.
     */
    public static Bitmap getImageBitmap(Activity activity, Uri currentPhotoUri){
        ContentResolver resolver = activity.getApplicationContext()
                .getContentResolver();
        try {
            InputStream stream = resolver.openInputStream(currentPhotoUri);
            Bitmap bitmap = BitmapFactory.decodeStream(stream);
            Objects.requireNonNull(stream).close();
            return bitmap;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * This function alert the user if he could lose some data.
     * @param context context of application.
     * @param activity activity of application.
     * @param newActivity activity to start.
     */
    public static void showAlert(Context context, AppCompatActivity activity, Class newActivity){
        new AlertDialog.Builder(context)
                .setTitle("Sei sicuro di voler abbandonare la schemata?")
                .setMessage("I dati raccolti finora verranno elimiati")
                .setNeutralButton("Annulla", null)
                .setPositiveButton("Ok", (dialog, which) -> Utility.launchActivity( activity, newActivity)).show();
    }

    /**
     * Returns date in string format
     * @param time time in millis
     * @return a time in string format HH:mm:ss
     */
    public static String getTimeFormat(long time){
        Date date = new Date(time);
        SimpleDateFormat formatter = new SimpleDateFormat("HH:mm:ss", Locale.ITALY);
        formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
        return formatter.format(date);
    }

    /**
     * Used to check if gps is enabled.
     * @param activity used for locationManager.
     * @return a boolean true if enabled.
     */
    public static boolean isGPSEnabled(Activity activity) {
        final LocationManager manager = (LocationManager) activity.getSystemService(Context.LOCATION_SERVICE);
        if (manager != null) {
            return manager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } else {
            return false;
        }
    }

    /**
     * This function ask the user to enabled GPS.
     * @param activity application activity.
     */
    public static void enabledGPS(Activity activity){
        final AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        final String action = Settings.ACTION_LOCATION_SOURCE_SETTINGS;
        final String message = "Il GPS è necessario per le funzionalità dell\'app. Vuoi aprire le impostazioni per la localizzazione?";
        builder.setMessage(message)
                .setPositiveButton("OK",
                        (d, id) -> {
                            activity.startActivity(new Intent(action));
                            d.dismiss();
                        })
                .setNegativeButton("Cancel",
                        (d, id) -> d.cancel());
        builder.create().show();
    }
}
