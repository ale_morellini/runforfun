package com.example.runforfun;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;

import com.example.runforfun.database.AppRepository;
import com.example.runforfun.database.User;
import com.example.runforfun.utilities.Utility;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public class UserProfileEditFragment extends Fragment implements BottomNavigationView.OnNavigationItemSelectedListener{

    static final String EDIT_PROFILE_TAG = "UserEditProfileFragment";
    private SharedPreferences sharedPreferences;
    private TextInputEditText height;
    private TextInputEditText weight;
    private TextInputEditText first_name;
    private TextInputEditText last_name;
    private TextInputEditText birth_date;
    private AutoCompleteTextView gender;
    private AppRepository appRepository;
    private ImageView imageView;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        return inflater.inflate(R.layout.fragment_user_profile_edit, container, false);
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        final UserActivity activity = (UserActivity) getActivity();
        if(activity!=null) {

            appRepository = new AppRepository(activity.getApplication());
            ExtendedFloatingActionButton saveButton = activity.findViewById(R.id.fab_saveButton);

            if(!checkFirstUse(activity)){

                //logic of save button. Just needs to update user info in DB
                saveButton.setOnClickListener(v -> {
                    if(validateUserInput()) {

                        Thread userThread = new Thread(() -> appRepository.updateUser(new User(Utility.USER_ID,String.valueOf(first_name.getText()),String.valueOf(last_name.getText()), Integer.parseInt(String.valueOf(height.getText())),
                                Integer.parseInt(String.valueOf(weight.getText())),
                                calculateBmi(Integer.parseInt(Objects.requireNonNull(height.getText()).toString()),Integer.parseInt(String.valueOf(weight.getText()))),
                                String.valueOf(birth_date.getText()), gender.getText().toString())));

                        userThread.start();

                        new DataBaseWorkerChecker(200, userThread) {
                            @Override
                            void action() {
                                Utility.replaceFragment(activity, R.id.fragment_container_user, new UserProfileFragment(), EDIT_PROFILE_TAG);
                            }
                        };

                    }else {
                        showMessage(getString(R.string.error),getString(R.string.error_user_empty),activity);
                    }
                });

            }else {

                //logic of save button. In this case has to insert a new user in DB
                saveButton.setOnClickListener(v -> {
                    if(validateUserInput()) {
                        setUserCreated();

                        Thread userThread = new Thread(() -> appRepository.addUser(new User(Utility.USER_ID,String.valueOf(first_name.getText()),String.valueOf(last_name.getText()), Integer.parseInt(String.valueOf(height.getText())),
                                Integer.parseInt(String.valueOf(weight.getText())),
                                calculateBmi(Integer.parseInt(Objects.requireNonNull(height.getText()).toString()),Integer.parseInt(String.valueOf(weight.getText()))),
                                String.valueOf(birth_date.getText()), gender.getText().toString())));

                        userThread.start();

                        new DataBaseWorkerChecker(200, userThread) {
                            @Override
                            void action() {
                                Utility.replaceFragment(activity, R.id.fragment_container_user, new UserProfileFragment(), EDIT_PROFILE_TAG);
                            }
                        };

                    }else {
                        showMessage(getString(R.string.error),getString(R.string.error_user_empty),activity);
                    }
                });

            }


            //add picture button logic
            activity.findViewById(R.id.addPictureImageView).setOnClickListener(v -> dispatchTakePictureIntent(activity));

            BottomNavigationView bottomNavigationView = activity.findViewById(R.id.bottom_nav_bar);
            bottomNavigationView.setOnNavigationItemSelectedListener(this);
            bottomNavigationView.setSelectedItemId(R.id.profilo);

            //This block of code sets up a dropdown menu for "sex" editText
            List<String> list = Arrays.asList(getString(R.string.gender_male), getString(R.string.gender_female),getString(R.string.gender_other));
            ArrayAdapter<String> adapter = new ArrayAdapter<>(requireContext(), R.layout.gender_list, list);
            gender = activity.findViewById(R.id.genderTextView);
            gender.setAdapter(adapter);


            //Just getting Ui ref
            height = activity.findViewById(R.id.heightTextView);
            weight = activity.findViewById(R.id.weightTextView);
            first_name = activity.findViewById(R.id.first_nameTextView);
            last_name = activity.findViewById(R.id.last_nameTextView);
            birth_date = activity.findViewById(R.id.runItemTimeTextView);

            imageView = activity.findViewById(R.id.profile_image_edit);
            setImageView(activity.getImageBitmap());

            birth_date.setInputType(InputType.TYPE_NULL);
            birth_date.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                DialogFragment newFragment = new DatePickerFragment(activity,birth_date);
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if(hasFocus) {
                        newFragment.show(activity.getSupportFragmentManager(), "datePicker");
                    } else {
                        newFragment.dismiss();
                    }
                }
            });


            appRepository.getUserInfo().observe(activity, this::displayUserData);
        }
    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        AppCompatActivity activity = (AppCompatActivity) getActivity();
        if(activity != null) {
            Context context = getContext();
            switch (item.getItemId()) {
                case R.id.attivita:
                    Utility.showAlert(context,activity,ActivitiesActivity.class);
                    return true;
                case R.id.corsa:
                    Utility.showAlert(context,activity,MainActivity.class);
                    return true;
                case R.id.profilo:
                    return true;
                default:
                    return false;
            }
        }
        return false;
    }

    /**
     * This function allows to validate user input. Should be called on
     * save button press.
     * @return true if all edit fields are correctly compiled
     */
    private boolean validateUserInput(){

        boolean validationFlag = true;

        if(Objects.requireNonNull(height.getText()).toString().isEmpty() || height.getText().toString().matches(".*[ABCDEFGH].*") ||
            Integer.parseInt(height.getText().toString()) >= 220 || Integer.parseInt(height.getText().toString()) <= 100){
            height.setError("Inserire un valore numerico valido");
            validationFlag = false;
        }
        if(Objects.requireNonNull(weight.getText()).toString().isEmpty() || weight.getText().toString().matches(".*[ABCDEFGH].*") ||
            Integer.parseInt(weight.getText().toString()) <= 40 || Integer.parseInt(weight.getText().toString()) >= 200){
            weight.setError("Inserire un valore numerico valido");
            validationFlag = false;
        }
        if(Objects.requireNonNull(first_name.getText()).toString().isEmpty() || first_name.getText().toString().matches(".*[0-9].*")){
            first_name.setError("Inserire nome corretto");
            validationFlag = false;
        }
        if(Objects.requireNonNull(last_name.getText()).toString().isEmpty() || last_name.getText().toString().matches(".*[0-9].*")){
            last_name.setError("Inserire cognome corretto");
            validationFlag = false;
        }
        return validationFlag;
    }

    /**
     * Sets user info in UserProfileEditFragment. (writes on GUI)
     * @param user User data
     */
    private void displayUserData(User user){
        if(user!=null){
            height.setText(String.valueOf(user.height));
            weight.setText(String.valueOf(user.weight));
            first_name.setText(user.firstName);
            last_name.setText(user.lastName);
            gender.setText(user.sex);
            birth_date.setText(user.birthdate);
        }
    }

    /**
     * Simple function to get image from camera. Checks for permission and then
     * sends an intent.
     * @param activity activity used to create intent.
     */
    private void dispatchTakePictureIntent(Activity activity) {
        if (ContextCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted
            ActivityCompat.requestPermissions(activity,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    Utility.REQUEST_IMAGE_CAPTURE);
        }else{
            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            // Ensure that there's a camera activity to handle the intent
            if (takePictureIntent.resolveActivity(activity.getPackageManager()) != null) {
                activity.startActivityForResult(takePictureIntent, Utility.REQUEST_IMAGE_CAPTURE);
            }
        }
    }

    /**
     * Private method to check if user has already been created
     * @param activity activity from which read user pref.
     * @return true if user exists
     */
    private boolean checkFirstUse(AppCompatActivity activity){

        sharedPreferences = activity.getSharedPreferences(Utility.KEY_FIRST_USE, Context.MODE_PRIVATE);

        //if first use...
        if(sharedPreferences.getBoolean(Utility.KEY_FIRST_USE,true)){
            showMessage(getString(R.string.welcome_title), getString(R.string.welcome_message), activity);
            return true;
        }
        return false;
    }

    /**
     * Shows an alert dialog.
     * @param title title of dialog
     * @param message message to show
     * @param activity activity from which
     */
    private void showMessage(String title,String message, AppCompatActivity activity)
    {
        AlertDialog.Builder builder=new AlertDialog.Builder(activity);
        builder.setCancelable(true);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton("OK", (dialog, which) -> {
        });
        builder.show();
    }

    /**
     * Private function to set that user has been created.
     */
    private void setUserCreated(){

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(Utility.KEY_FIRST_USE, false);
        editor.apply();
    }



    /**
     * Method use to set the bitmap that will be displayed on the ImageView
     * @param bitmap bitmap representing the picture taken.
     */
    private void setImageView(Bitmap bitmap) {
        if(bitmap!=null){
            imageView.setImageBitmap(bitmap);
        }
    }

    /**
     * Simple method to calculate user BMI.
     * @param height height
     * @param weight weight
     * @return bmi value
     */
    private int calculateBmi(int height, int weight){
        float meters = (float) height/100;
        return (int) (weight/(meters*meters));
    }

}
