package com.example.runforfun.utilities;


import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

public class RunUtilities {

    private final Map<Integer, Double> metMap = new HashMap<>();
    private boolean isFair = true;
    private static final double TEMP_WEIGHT = 70.0;
    private static final int MIN_KEY = 1;
    private static final int MAX_KEY = 22;
    private static final int MOLT_CONST = 60;
    private static final double VEL_CONST = 3.6;


    /**
     * Public constructor. The metMap is filled with met value as value and speed as key
     */
    public RunUtilities() {
        metMap.put(1, 2.0);
        metMap.put(2, 2.0);
        metMap.put(3, 3.0);
        metMap.put(4, 3.3);
        metMap.put(5, 3.5);
        metMap.put(6, 6.0);
        metMap.put(7, 7.0);
        metMap.put(8, 8.3);
        metMap.put(9, 9.8);
        metMap.put(10, 10.3);
        metMap.put(11, 11.0);
        metMap.put(12, 11.8);
        metMap.put(13, 12.3);
        metMap.put(14, 11.8);
        metMap.put(15, 13.5);
        metMap.put(16, 14.5);
        metMap.put(17, 16.0);
        metMap.put(18, 17.0);
        metMap.put(19, 19.0);
        metMap.put(20, 19.8);
        metMap.put(21, 21.0);
        metMap.put(22, 23.0);
    }


    /**
     * Calculate burned calories from speed and MET value from our HashMap
     *
     * @param speed average speed for the last few seconds
     * @return burned calories
     */
    int calculateCalories(double speed) {
        int rounded = new BigDecimal(speed).intValue();
        if (rounded >= MIN_KEY && rounded <= MAX_KEY && isFair && metMap.get(rounded) != null) {
            double metValue = metMap.get(rounded);
            double cal= new BigDecimal(metValue * TEMP_WEIGHT).doubleValue() * 5 / 3600;
            return (int) Math.floor(cal);
        } else {
            if(rounded > MAX_KEY){
                isFair=false;
            }
            return 0;
        }
    }

    /**
     * Calculate running pace
     * @param speed speed
     * @return Pace in the format x'y''
     */
    public String calculatePace(double speed) {
        if (speed >= 4*MIN_KEY) {
            double rawTime = MOLT_CONST / speed;
            double seconds = Math.floor(MOLT_CONST * (rawTime - Math.floor(rawTime)));
            double minutes = Math.floor(rawTime);
            return (int) minutes + "'" + (int) seconds + "''";
        }else{
            return "0'0''";
        }
    }

    /**
     *
     * @param distance total distance in km
     * @param duration total duration in millis
     * @return Pace in the format x'y''
     */
    public String calculateAveragePace(double distance, long duration) {
        double distInMeter = distance * 1000;
        double timeInMin = duration / 1000.0;
        double rawTime = (MOLT_CONST*timeInMin)/(distInMeter*VEL_CONST);
        double seconds = Math.floor(MOLT_CONST*(rawTime - Math.floor(rawTime)));
        double minutes = Math.floor(rawTime);
        return (int) minutes + "'" + (int)seconds + "''";
    }

    /**
     * Function that calculate distance between two given point with latitude and longitude.
     * @param srcLat first latitude.
     * @param srcLng first longitude.
     * @param desLat second latitude.
     * @param desLng second longitude.
     * @return distance
     */
    double distanceBetweenTwoPoint(double srcLat, double srcLng, double desLat, double desLng) {
        double earthRadius = 6369.63;
        double dLat = Math.toRadians(desLat - srcLat);
        double dLng = Math.toRadians(desLng - srcLng);
        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2)
                + Math.cos(Math.toRadians(srcLat))
                * Math.cos(Math.toRadians(desLat)) * Math.sin(dLng / 2)
                * Math.sin(dLng / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        return earthRadius * c;
    }

}
