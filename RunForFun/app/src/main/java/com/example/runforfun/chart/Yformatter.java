package com.example.runforfun.chart;

import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.formatter.ValueFormatter;

/**
 * Not used but could be useful. Simple formatter for Y axis in chart
 */
public class Yformatter extends ValueFormatter {

    private String symbol = "";

    Yformatter(String symbol){
        this.symbol = symbol;
    }

    @Override
    public String getAxisLabel(float value, AxisBase axis) {

        return value + " " + symbol;
    }

}
