package com.example.runforfun;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.example.runforfun.utilities.Utility;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.slider.Slider;
import com.google.android.material.switchmaterial.SwitchMaterial;

import java.util.ArrayList;
import java.util.List;


public class UserProfileSettingsFragment extends Fragment implements BottomNavigationView.OnNavigationItemSelectedListener {

    private static final String USER_PROFILE_TAG = "userProfile";

    private SwitchMaterial switchNotificationDistance;
    private SwitchMaterial switchNotificationPause;
    private Slider pauseTimeSlider;
    private Slider distanceSlider;
    private List<SharedPreferences> sharedPreferencesList = new ArrayList<>();
    private boolean distanceNotificationActive;
    private boolean pauseTimeNotificationActive;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        return inflater.inflate(R.layout.fragment_user_profile_settings, container,false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        final UserActivity activity = (UserActivity) getActivity();
        if(activity!=null){

            BottomNavigationView bottomNavigationView = activity.findViewById(R.id.bottom_nav_bar);
            bottomNavigationView.setOnNavigationItemSelectedListener(this);
            bottomNavigationView.setSelectedItemId(R.id.profilo);

            switchNotificationDistance = activity.findViewById(R.id.switchNotificationDistance);
            distanceSlider = activity.findViewById(R.id.distanceSlider);
            switchNotificationDistance.setOnClickListener(v -> {
                if(switchNotificationDistance.isChecked()){
                    distanceSlider.setEnabled(true);
                    distanceSlider.setClickable(true);
                    distanceSlider.setActivated(true);
                    distanceNotificationActive = true;
                }else {
                    distanceSlider.setEnabled(false);
                    distanceSlider.setClickable(false);
                    distanceSlider.setActivated(false);
                    distanceNotificationActive = false;
                }
            });


            switchNotificationPause = activity.findViewById(R.id.switchNotificationPause);
            pauseTimeSlider = activity.findViewById(R.id.pauseTimeSlider);
            switchNotificationPause.setOnClickListener(v -> {
                if(switchNotificationPause.isChecked()){
                    pauseTimeSlider.setEnabled(true);
                    pauseTimeSlider.setClickable(true);
                    pauseTimeSlider.setActivated(true);
                    pauseTimeNotificationActive = true;
                }else {
                    pauseTimeSlider.setEnabled(false);
                    pauseTimeSlider.setClickable(false);
                    pauseTimeSlider.setActivated(false);
                    pauseTimeNotificationActive = false;
                }
            });

            activity.findViewById(R.id.fab_saveButton).setOnClickListener(v -> setSettings(activity));

            loadSettings(activity);
        }
    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        AppCompatActivity activity = (AppCompatActivity) getActivity();
        if(activity != null) {
            switch (item.getItemId()) {
                case R.id.attivita:
                    Utility.launchActivity(activity, ActivitiesActivity.class);
                    return true;
                case R.id.corsa:
                    Utility.launchActivity(activity, MainActivity.class);
                    return true;
                case R.id.profilo:
                    return true;
                default:
                    return false;
            }
        }
        return false;
    }

    /**
     * Private method to load user default settings.
     * @param activity activity used to load SharedPref.
     */
    private void loadSettings(Activity activity){


        sharedPreferencesList.add(activity.getSharedPreferences(Utility.KEY_NOTIFICATION_DIST_TRIGGER, Context.MODE_PRIVATE));
        sharedPreferencesList.add(activity.getSharedPreferences(Utility.KEY_NOTIFICATION_PAUSE_TRIGGER, Context.MODE_PRIVATE));
        sharedPreferencesList.add(activity.getSharedPreferences(Utility.KEY_NOTIFICATION_DIST_ACTIVE,Context.MODE_PRIVATE));
        sharedPreferencesList.add(activity.getSharedPreferences(Utility.KEY_NOTIFICATION_PAUSE_ACTIVE,Context.MODE_PRIVATE));

        if(sharedPreferencesList.get(0)!=null){
            int distanceNotificationSetting = sharedPreferencesList.get(0).getInt(Utility.KEY_NOTIFICATION_DIST_TRIGGER, Utility.MINIMUM_DISTANCE_TRIGGER);
            distanceSlider.setValue(distanceNotificationSetting);
        }

        if(sharedPreferencesList.get(1)!=null){
            long pauseTimeNotificationSetting = sharedPreferencesList.get(1).getInt(Utility.KEY_NOTIFICATION_DIST_TRIGGER, Utility.MINIMUM_PAUSE_TIME_TRIGGER);
            pauseTimeSlider.setValue(pauseTimeNotificationSetting);
        }

        if(sharedPreferencesList.get(2)!=null){
            distanceNotificationActive = sharedPreferencesList.get(2).getBoolean(Utility.KEY_NOTIFICATION_DIST_ACTIVE, false);
            switchNotificationDistance.setChecked(distanceNotificationActive);
            distanceSlider.setEnabled(distanceNotificationActive);
        }

        if(sharedPreferencesList.get(3)!=null){
            pauseTimeNotificationActive = sharedPreferencesList.get(3).getBoolean(Utility.KEY_NOTIFICATION_PAUSE_ACTIVE, false);
            switchNotificationPause.setChecked(pauseTimeNotificationActive);
            pauseTimeSlider.setEnabled(pauseTimeNotificationActive);
        }
    }

    /**
     * Private method to save user settings. After that setting fragment is closed.
     */
    private void setSettings(AppCompatActivity activity){

        SharedPreferences.Editor editor = sharedPreferencesList.get(0).edit();
        editor.putInt(Utility.KEY_NOTIFICATION_DIST_TRIGGER, (int) distanceSlider.getValue());
        editor.apply();

        SharedPreferences.Editor editorPause = sharedPreferencesList.get(1).edit();
        editorPause.putLong(Utility.KEY_NOTIFICATION_PAUSE_TRIGGER, (int) pauseTimeSlider.getValue());
        editorPause.apply();

        SharedPreferences.Editor editorDistanceActive = sharedPreferencesList.get(2).edit();
        editorDistanceActive.putBoolean(Utility.KEY_NOTIFICATION_DIST_ACTIVE, distanceNotificationActive);
        editorDistanceActive.apply();

        SharedPreferences.Editor editorPauseTimeActive = sharedPreferencesList.get(3).edit();
        editorPauseTimeActive.putBoolean(Utility.KEY_NOTIFICATION_PAUSE_ACTIVE, pauseTimeNotificationActive);
        editorPauseTimeActive.apply();

        Utility.replaceFragment(activity,R.id.fragment_container_user,new UserProfileFragment(),USER_PROFILE_TAG);

    }

}
