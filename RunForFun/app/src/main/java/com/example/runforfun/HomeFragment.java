package com.example.runforfun;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.example.runforfun.utilities.PermissionUtilities;
import com.example.runforfun.utilities.Utility;
import com.example.runforfun.viewModel.UserViewModel;
import com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.mapbox.mapboxsdk.Mapbox;
import com.mapbox.mapboxsdk.location.LocationComponent;
import com.mapbox.mapboxsdk.location.LocationComponentActivationOptions;
import com.mapbox.mapboxsdk.location.LocationComponentOptions;
import com.mapbox.mapboxsdk.location.OnCameraTrackingChangedListener;
import com.mapbox.mapboxsdk.location.modes.CameraMode;
import com.mapbox.mapboxsdk.location.modes.RenderMode;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;
import com.mapbox.mapboxsdk.maps.Style;

public class HomeFragment extends Fragment implements BottomNavigationView.OnNavigationItemSelectedListener,
        OnMapReadyCallback, OnCameraTrackingChangedListener {

    private MapboxMap mapboxMap;
    private MapView mapView;
    private LocationComponent locationComponent;
    private boolean isInTrackingMode;
    private UserViewModel model;
    private String calories;
    private String distance;
    private String time;
    private static final int DEFAULT_PROGRESS = 0;
    private static final String RUN_FRAGMENT_TAG = "RunFragment";


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        Activity activity = getActivity();
        if(activity != null) {
            Mapbox.getInstance(activity.getApplicationContext(), getString(R.string.mapbox_access_token));
        }
        return inflater.inflate(R.layout.fragment_home, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        final AppCompatActivity activity = (AppCompatActivity) getActivity();
        if(activity!=null){

            //setting the navigation Bar
            BottomNavigationView bottomNavigationView = activity.findViewById(R.id.bottom_nav_bar);
            bottomNavigationView.setOnNavigationItemSelectedListener(this);
            bottomNavigationView.setSelectedItemId(R.id.corsa);

            final SeekBar recapSeekBar = activity.findViewById(R.id.recapSeekBar);
            final TextView recapTextView = activity.findViewById(R.id.recapValueTextView);

            //setting the start button
            ExtendedFloatingActionButton startButton = activity.findViewById(R.id.fab_ok);
            startButton.setOnClickListener(v -> {
                if(Utility.isGPSEnabled(activity)){
                    if (PermissionUtilities.checkPermissions(activity)) {
                        Utility.replaceFragment(activity, R.id.fragment_container, new RunFragment(), RUN_FRAGMENT_TAG);
                    } else {
                        PermissionUtilities.requestLocationPermissions(activity);
                    }
                }else{
                    Utility.enabledGPS(activity);
                }

            });


            mapView = activity.findViewById(R.id.mapView);
            mapView.onCreate(savedInstanceState);
            mapView.getMapAsync(this);


            if(!Utility.isGPSEnabled(activity)) {
                Utility.enabledGPS(activity);
            }
            if (!PermissionUtilities.checkPermissions(activity)) {
                PermissionUtilities.requestLocationPermissions(activity);
            }

            model = new ViewModelProvider(this).get(UserViewModel.class);


            //working on db on a separate thread
            Thread dataBaseWorker;
            dataBaseWorker = new Thread(() -> {
                calories = model.getLastMonthTotalCalories();
                distance = model.getLastMonthTotalDistance();
                time = model.getLastMonthTotalTime();

            });
            dataBaseWorker.start();

            //once database worker thread has done just set info
            new DataBaseWorkerChecker(500, dataBaseWorker) {
                @Override
                void action() {
                    recapSeekBar.setProgress(DEFAULT_PROGRESS);
                    setRecapTextView(recapSeekBar.getProgress(),recapTextView);

                    recapSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                        @Override
                        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                            setRecapTextView(seekBar.getProgress(),recapTextView);
                        }

                        @Override
                        public void onStartTrackingTouch(SeekBar seekBar) {

                        }

                        @Override
                        public void onStopTrackingTouch(SeekBar seekBar) {

                        }
                    });
                }
            };
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        AppCompatActivity activity = (AppCompatActivity) getActivity();
        if(activity!=null){
            switch (item.getItemId()) {
                case R.id.profilo:
                    Utility.launchActivity( activity, UserActivity.class);
                    return true;
                case R.id.attivita:
                    Utility.launchActivity( activity, ActivitiesActivity.class);
                    return true;
                case R.id.corsa:
                    return true;
                default:
                    return false;
            }
        }
        return false;
    }

    /**
     * Private method to enable the location component.
     * @param loadedMapStyle requires a map style to be applied.
     */
    private void enableLocationComponent(@NonNull Style loadedMapStyle) {
        Context context = getContext();
        if(context != null) {
            LocationComponentOptions customLocationComponentOptions = LocationComponentOptions.builder(context)
                    .elevation(5)
                    .accuracyAlpha(.6f)
                    .foregroundTintColor(Color.BLUE)
                    .build();

            // Get an instance of the component
            locationComponent = mapboxMap.getLocationComponent();

            LocationComponentActivationOptions locationComponentActivationOptions =
                    LocationComponentActivationOptions.builder(context, loadedMapStyle)
                            .locationComponentOptions(customLocationComponentOptions)
                            .build();

            // Activate with options
            locationComponent.activateLocationComponent(locationComponentActivationOptions);
            locationComponent.setLocationComponentEnabled(true);

            locationComponent.setCameraMode(CameraMode.TRACKING);
            locationComponent.setRenderMode(RenderMode.COMPASS);

            locationComponent.addOnCameraTrackingChangedListener(this);

            Activity activity = getActivity();
            if (activity != null) {
                activity.findViewById(R.id.back_to_camera_tracking_mode).setOnClickListener(view -> {
                    if (!isInTrackingMode) {
                        isInTrackingMode = true;
                        locationComponent.setCameraMode(CameraMode.TRACKING);
                        locationComponent.zoomWhileTracking(16f);
                    }
                });
            }
        }
    }


    @Override
    public void onCameraTrackingDismissed() {
        isInTrackingMode = false;
    }

    @Override
    public void onCameraTrackingChanged(int currentMode) {

    }

    @Override
    public void onMapReady(@NonNull MapboxMap mapboxMap) {
        this.mapboxMap = mapboxMap;
        Handler handler = new Handler();
        handler.postDelayed(() -> {
            if(PermissionUtilities.checkPermissions(getActivity())){
                this.mapboxMap.setStyle(Style.LIGHT, this::enableLocationComponent);
            }else {
                onMapReady(mapboxMap);
            }
        }, 500);

    }

    /**
     * Set the textView with the right string for each seekBar progress value.
     *
     * @param progress SeekBar progress.
     * @param recapTextView The TextView to be filled.
     */
    private void setRecapTextView(int progress, TextView recapTextView){
        Activity activity = getActivity();
        if(activity != null) {
            switch (progress) {
                case 0:
                    recapTextView.setText(activity.getString(R.string.km, String.valueOf(distance)));
                    break;
                case 1:
                    recapTextView.setText(activity.getString(R.string.min, String.valueOf(time)));
                    break;
                case 2:
                    recapTextView.setText(calories);
                    break;
            }
        }
    }

}
