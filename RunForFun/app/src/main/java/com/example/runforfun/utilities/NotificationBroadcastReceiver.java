package com.example.runforfun.utilities;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import com.example.runforfun.MainActivity;
import com.example.runforfun.R;

import static com.mapbox.mapboxsdk.Mapbox.getApplicationContext;

/**
 * This class is a receiver that builds and sends notifications. It operates
 * when receives a specific intent.
 */
public class NotificationBroadcastReceiver extends BroadcastReceiver {

    public static final String ACTION_NOTIFICATION = "com.example.runforfun.action.notification";
    public static final String NOTIFICATION_TAG = "Notification receiver";
    private final static String notification_channel = "Notifiche corsa";
    private final static String notification_channel_high = "Notifiche pausa";


    //constructor creates also the notification channels
    public NotificationBroadcastReceiver(){
        super();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel =
                    new NotificationChannel(notification_channel, notification_channel, NotificationManager.IMPORTANCE_LOW);

            NotificationChannel highChannel =
                    new NotificationChannel(notification_channel_high, notification_channel_high, NotificationManager.IMPORTANCE_HIGH);

            //description for the channel
            String description = "A channel which shows notifications about run distance";
            channel.setDescription(description);

            String descriptionHigh = "A channel which shows pause notification";
            channel.setDescription(descriptionHigh);

            NotificationManager notificationManager = (NotificationManager) getApplicationContext().
                    getSystemService(Context.NOTIFICATION_SERVICE);
            if (notificationManager != null) {
                notificationManager.createNotificationChannel(channel);
                notificationManager.createNotificationChannel(highChannel);
            }
        }
    }


    @Override
    public void onReceive(Context context, Intent intent)
    {
        Log.d(NOTIFICATION_TAG, "Sending new notification");
        String distance;
        String time;

        Bundle extras = intent.getExtras();
        if(extras!=null){
            distance = extras.getString("distance");
            time = extras.getString("time");

            Intent goBack = new Intent(context, MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, goBack, 0);

            if(distance!=null){

                NotificationCompat.Builder notificationBuilder =
                        new NotificationCompat.Builder(context, notification_channel)
                                .setSmallIcon(R.drawable.icon)
                                .setContentTitle(context.getString(R.string.notification_title))
                                .setContentText(context.getString(R.string.notification_value, distance))
                                .setAutoCancel(true)
                                .setContentIntent(pendingIntent)
                                .setPriority(NotificationCompat.PRIORITY_DEFAULT);

                //trigger the notification
                NotificationManagerCompat notificationManager =
                        NotificationManagerCompat.from(context);
                notificationManager.notify(1, notificationBuilder.build());
            }

            if(time!=null){

                NotificationCompat.Builder notificationBuilder =
                        new NotificationCompat.Builder(context, notification_channel_high)
                                .setSmallIcon(R.drawable.icon)
                                .setContentTitle(context.getString(R.string.notification_pause_title))
                                .setContentText(context.getString(R.string.notification_pause_value, time))
                                .setAutoCancel(true)
                                .setContentIntent(pendingIntent)
                                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                                .setPriority(NotificationCompat.PRIORITY_HIGH);

                //trigger the notification
                NotificationManagerCompat notificationManager =
                        NotificationManagerCompat.from(context);
                notificationManager.notify(2, notificationBuilder.build());
            }
        }
    }
}

