package com.example.runforfun.database;

import android.app.Application;

import androidx.lifecycle.LiveData;

import java.util.List;

public class AppRepository {
    private  RunDAO runDAO;
    private  UserDAO userDAO;
    private  LiveData<User> userLiveData;

    public AppRepository(Application application) {
        AppDatabase db = AppDatabase.getDatabase(application);
        runDAO = db.runDAO();
        userDAO = db.userDAO();
        userLiveData = userDAO.getUserInfo();
    }

    public LiveData<List<RunItem>> getAll_run_items(){return  runDAO.getAllRunItems();}

    //maybe could be useful later
    public List<RunItemComplete> getAll_runs_complete(){return runDAO.getAllRunsComplete();}

    public RunItem getLastRunItem(){return runDAO.getLastRunItem();}

    public double getTotalAvgSpeed(){return  runDAO.getTotalAvgSpeed();}

    public double getTotalDistance(){return runDAO.getTotalDistance();}

    public double getTotalTime(){return  runDAO.getTotalTime();}

    public int getTotalCalories(){return runDAO.getTotalCalories();}

    public int getLastMonthTotalCalories(){return  runDAO.getLastMonthTotalCalories();}

    public long getLastMonthTotalTime(){return  runDAO.getLastMonthTotalTime();}

    public double getLastMonthTotalDistance(){return runDAO.getLastMonthTotalDistance();}

    public void deleteDetailItems(int id){runDAO.deleteDetailItems(id);}

    public void deleteRunItem(int id){runDAO.deleteRunItem(id);}

    public void addRunItem(final RunItem runItem){
        AppDatabase.databaseWriteExecutor.execute(() -> runDAO.addRunItem(runItem));
    }

    public void addRunDetailItem(final RunDetailItem runDetailItem){
        AppDatabase.databaseWriteExecutor.execute(() -> runDAO.addRunDetailItem(runDetailItem));
    }

    public RunItemComplete getCompleteRunById(int id){return runDAO.getCompleteRunById(id);}

    //USER methods
    public void addUser(final User user){
        AppDatabase.databaseWriteExecutor.execute(() -> userDAO.addUser(user));
    }

    public void updateUser(final User user){
        AppDatabase.databaseWriteExecutor.execute(() -> userDAO.updateUser(user));
    }

    public LiveData<User> getUserInfo(){return userLiveData;}

}
