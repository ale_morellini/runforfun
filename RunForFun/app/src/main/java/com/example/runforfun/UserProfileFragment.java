package com.example.runforfun;

import android.app.Activity;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.example.runforfun.database.User;
import com.example.runforfun.utilities.Utility;
import com.example.runforfun.viewModel.UserViewModel;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;


public class UserProfileFragment extends Fragment implements BottomNavigationView.OnNavigationItemSelectedListener {

    private static final String EDIT_PROFILE_TAG = "editProfile";
    private static final String SETTINGS_PROFILE_TAG = "settingsProfile";
    private UserViewModel model;
    private TextView age;
    private TextView height;
    private TextView weight;
    private TextView bmi;
    private TextView fullname;
    private ImageView imageView;
    private TextView distance;
    private TextView time;
    private TextView calories;
    private TextView avgSpeed;
    private String distanceValue;
    private String timeValue;
    private String avgSpeedValue;
    private String caloriesValue;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        return inflater.inflate(R.layout.fragment_user_profile, container,false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        final UserActivity activity = (UserActivity) getActivity();
        if(activity!=null){

            activity.findViewById(R.id.editButtonImageView).setOnClickListener(v -> Utility.replaceFragment(activity,R.id.fragment_container_user,new UserProfileEditFragment(),EDIT_PROFILE_TAG));
            activity.findViewById(R.id.settingsButtonImageView).setOnClickListener(v -> Utility.replaceFragment(activity,R.id.fragment_container_user,new UserProfileSettingsFragment(),SETTINGS_PROFILE_TAG));


            BottomNavigationView bottomNavigationView = activity.findViewById(R.id.bottom_nav_bar);
            bottomNavigationView.setOnNavigationItemSelectedListener(this);
            bottomNavigationView.setSelectedItemId(R.id.profilo);

            age = activity.findViewById(R.id.ageValueTextView);
            height = activity.findViewById(R.id.heightValueTextView);
            weight = activity.findViewById(R.id.weightValueTextView);
            bmi = activity.findViewById(R.id.bmiValueTextView);
            fullname = activity.findViewById(R.id.fullnameTextView);
            imageView = activity.findViewById(R.id.profile_image);

            //user stats
            distance = activity.findViewById(R.id.kilometersValueTextView);
            avgSpeed = activity.findViewById(R.id.avg_velValueTextView);
            calories = activity.findViewById(R.id.caloriesValueTextView);
            time = activity.findViewById(R.id.timeValueTextView);

            setImageView(activity.getImageBitmap());

            model = new ViewModelProvider(this).get(UserViewModel.class);

            model.getUserInfo().observe(activity, user -> {
                clearUserData();
                if(user!=null){
                    displayUserData(activity,user);
                }
            });

            Thread dataBaseWorker;
            dataBaseWorker = new Thread(() -> {
                avgSpeedValue = model.getTotalAvgSpeed();
                distanceValue = model.getTotalDistance();
                caloriesValue = model.getTotalCalories();
                timeValue = model.getTotalTime();
            });

            dataBaseWorker.start();

            new DataBaseWorkerChecker(500, dataBaseWorker) {
                @Override
                void action() {
                    avgSpeed.setText(activity.getString(R.string.km_h, avgSpeedValue));
                    distance.setText(activity.getString(R.string.km, distanceValue));
                    calories.setText(caloriesValue);
                    time.setText(timeValue);
                }
            };
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        AppCompatActivity activity = (AppCompatActivity) getActivity();
        if(activity != null) {
            switch (item.getItemId()) {
                case R.id.attivita:
                    Utility.launchActivity(activity, ActivitiesActivity.class);
                    return true;
                case R.id.corsa:
                    Utility.launchActivity(activity, MainActivity.class);
                    return true;
                case R.id.profilo:
                    return true;
                default:
                    return false;
            }
        }
        return false;
    }

    /**
     * Sets user info in UserFragment. (writes on GUI)
     * @param user User data
     */
    private void displayUserData(Activity activity, User user) {
        height.setText(String.valueOf(user.height));
        weight.setText(String.valueOf(user.weight));
        fullname.setText(activity.getString(R.string.fullname, user.firstName, user.lastName));
        bmi.setText(String.valueOf(user.bmi));

        DateFormat birth = new SimpleDateFormat("dd/MM/yy", Locale.ITALY);
        Date birthdate;
        try {
            birthdate = birth.parse(user.getBirthDate());
            assert birthdate != null;
            long diff = new Date().getTime() - birthdate.getTime();
            long days = diff / (1000*60*60*24);
            age.setText(String.valueOf(days/365));
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    /**
     * Method to clear displayed user info
     */
    private void clearUserData(){
        age.setText("");
        height.setText("");
        weight.setText("");
        bmi.setText("");
        fullname.setText("");
    }

    /**
     * Method use to set the bitmap that will be displayed on the ImageView
     * @param bitmap bitmap representing the picture taken.
     */
    private void setImageView(Bitmap bitmap) {
        if(bitmap!=null){
            imageView.setImageBitmap(bitmap);
        }
    }
}
