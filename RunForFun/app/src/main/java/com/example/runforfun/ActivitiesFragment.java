package com.example.runforfun;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.RecyclerView;

import com.example.runforfun.database.RunItem;
import com.example.runforfun.recyclerView.ActivitiesAdapter;
import com.example.runforfun.recyclerView.OnItemListener;
import com.example.runforfun.utilities.PermissionUtilities;
import com.example.runforfun.utilities.Utility;
import com.example.runforfun.viewModel.ActivitiesViewModel;
import com.google.android.material.bottomnavigation.BottomNavigationView;


public class ActivitiesFragment extends Fragment implements OnItemListener, BottomNavigationView.OnNavigationItemSelectedListener {

    private static final String ACTIVITIES_DETAIL_FRAGMENT_TAG = "ActivitiesDetailFragment";
    static final String  CALLER_ITEM_ID = "callerId";

    private ActivitiesAdapter adapter;
    private RecyclerView recyclerView;
    private ConstraintLayout emptyLayout;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        return inflater.inflate(R.layout.fragment_activities, container,false);

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        final AppCompatActivity activity = (AppCompatActivity) getActivity();
        if(activity!=null){

            setRecyclerView(activity);
            emptyLayout = activity.findViewById(R.id.emptyLayout);


            ActivitiesViewModel model = new ViewModelProvider(this).get(ActivitiesViewModel.class);

            model.getAllRunItems().observe(activity, runItems -> {
                if(runItems.size() > 0){
                    emptyLayout.setVisibility(View.INVISIBLE);
                    recyclerView.setVisibility(View.VISIBLE);
                    adapter.setData(runItems);
                }else {
                    recyclerView.setVisibility(View.INVISIBLE);
                    emptyLayout.setVisibility(View.VISIBLE);
                }
            });

            BottomNavigationView bottomNavigationView = activity.findViewById(R.id.bottom_nav_bar);
            bottomNavigationView.setOnNavigationItemSelectedListener(this);
            bottomNavigationView.setSelectedItemId(R.id.attivita);

            if(!PermissionUtilities.checkPermissions(activity)){
                PermissionUtilities.requestLocationPermissions(activity);
            }
        }
    }

    /**
     * Method to set the RecyclerView and the relative adapter
     * @param activity the current activity
     */
    private void setRecyclerView(final Activity activity){
        recyclerView = activity.findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);
        final OnItemListener listener = this;
        adapter = new ActivitiesAdapter(listener, activity);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        AppCompatActivity activity = (AppCompatActivity) getActivity();
        if(activity!=null){
            switch (item.getItemId()) {
                case R.id.profilo:
                    Utility.launchActivity(activity, UserActivity.class);
                    return true;
                case R.id.corsa:
                    Utility.launchActivity(activity, MainActivity.class);
                case R.id.attivita:
                    return true;
                default:
                    return false;
            }
        }
        return false;
    }

    /**
     * Method called when the user click a card
     * @param position position of the item clicked
     */
    @Override
    public void onItemClick(final int position) {
        AppCompatActivity activity = (AppCompatActivity) getActivity();
        if(activity!=null){
            ActivitiesDetailFragment fragment = new ActivitiesDetailFragment();

            RunItem item = adapter.getRunItem(position);
            Bundle bundle = new Bundle();
            bundle.putInt(CALLER_ITEM_ID, item.getRun_item_id());
            fragment.setArguments(bundle);
            Utility.replaceFragment(activity,R.id.fragment_container_activities,fragment, ACTIVITIES_DETAIL_FRAGMENT_TAG);
        }
    }
}
