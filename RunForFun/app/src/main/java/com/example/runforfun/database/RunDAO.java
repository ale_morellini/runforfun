package com.example.runforfun.database;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Transaction;

import java.util.List;

@Dao
public interface RunDAO {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void addRunItem(RunItem runItem);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void addRunDetailItem(RunDetailItem runDetailItem);

    @Transaction
    @Query("DELETE from RunDetailItem WHERE runId == :runItemId")
    void deleteDetailItems(int runItemId);

    @Transaction
    @Query("DELETE from RunItem WHERE run_item_id == :runItemId")
    void deleteRunItem(int runItemId);

    @Transaction
    @Query("SELECT * from RunItem ORDER BY run_item_id DESC")
    //should use to get a preview of all runs
    LiveData<List<RunItem>> getAllRunItems();

    @Transaction
    @Query("SELECT * from RunItem")
    //should use to get all runs with all relative info
    List<RunItemComplete> getAllRunsComplete();

    @Transaction
    @Query("SELECT * from RunItem ORDER BY run_item_id DESC LIMIT 1")
    RunItem getLastRunItem();


    //returns a specific complete run by id
    @Transaction
    @Query("SELECT * from RunItem WHERE run_item_id = :id ")
    //returns a specific complete run by id
    RunItemComplete getCompleteRunById(int id);

    //Below there are queries for User profile Stats//
    @Transaction
    @Query("SELECT AVG(avg_speed) from RunItem")
        //should use to get all runs with all relative info
    double getTotalAvgSpeed();

    @Transaction
    @Query("SELECT SUM(distance) from RunItem")
        //should use to get all runs with all relative info
    double getTotalDistance();

    @Transaction
    @Query("SELECT SUM(time) from RunItem")
        //should use to get all runs with all relative info
    double getTotalTime();

    @Transaction
    @Query("SELECT SUM(calories) from RunItem")
        //should use to get all runs with all relative info
    int getTotalCalories();

    //Last month queries
    @Transaction
    @Query("SELECT SUM(calories) FROM RunItem WHERE strftime('%m',datetime('now', 'localtime')) == strftime('%m',RunItem.date)")
    int getLastMonthTotalCalories();

    @Transaction
    @Query("SELECT SUM(time) FROM RunItem WHERE strftime('%m',datetime('now', 'localtime')) == strftime('%m',RunItem.date)")
    long getLastMonthTotalTime();

    @Transaction
    @Query("SELECT SUM(distance) FROM RunItem WHERE strftime('%m',datetime('now', 'localtime')) == strftime('%m',RunItem.date)")
    double getLastMonthTotalDistance();
}



