package com.example.runforfun.viewModel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.example.runforfun.database.AppRepository;
import com.example.runforfun.database.RunItem;

import java.util.List;

public class ActivitiesViewModel extends AndroidViewModel {

    private AppRepository appRepository;
    private LiveData<List<RunItem>> runsLiveData;

    public ActivitiesViewModel(@NonNull Application application) {
        super(application);
        appRepository = new AppRepository(application);
    }

    public LiveData<List<RunItem>> getAllRunItems(){
        if (runsLiveData == null) {
            runsLiveData = appRepository.getAll_run_items();
        }
        return runsLiveData;}

}
