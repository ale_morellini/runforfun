package com.example.runforfun;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.example.runforfun.database.AppRepository;
import com.example.runforfun.database.RunDetailItem;
import com.example.runforfun.database.RunItem;
import com.example.runforfun.utilities.RunUtilities;
import com.example.runforfun.utilities.Utility;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class RecapFragment extends Fragment implements BottomNavigationView.OnNavigationItemSelectedListener{

    private List<RunDetailItem> detailItemList = new ArrayList<>();
    private RunUtilities runUtilities = new RunUtilities();
    private AppRepository appRepository;
    private RunDetailItemSingleton runDetailItemSingleton;
    private ProgressBar progressBar;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        return inflater.inflate(R.layout.fragment_recap, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


        final AppCompatActivity activity = (AppCompatActivity) getActivity();
        if(activity!=null){
            //Setting Bottom Navigation bar
            BottomNavigationView bottomNavigationView = activity.findViewById(R.id.bottom_nav_bar);
            bottomNavigationView.setOnNavigationItemSelectedListener(this);
            bottomNavigationView.setSelectedItemId(R.id.corsa);

            appRepository = new AppRepository(activity.getApplication());
            runDetailItemSingleton = RunDetailItemSingleton.getInstance();
            detailItemList = runDetailItemSingleton.getRunItems();

            progressBar = activity.findViewById(R.id.progressBar);

            Bundle extras = activity.getIntent().getExtras();
            if(extras != null){
                final int calories = extras.getInt("calories");
                final double distance = BigDecimal.valueOf(extras.getDouble("distance")).setScale(2,BigDecimal.ROUND_UP).doubleValue() ;
                final long durationValue = extras.getLong("durationValue");
                String duration = extras.getString("duration");
                final double avgSpeed = BigDecimal.valueOf(calculateAvgSpeed()).setScale(2, BigDecimal.ROUND_UP).doubleValue();
                final String avgPace = runUtilities.calculateAveragePace(distance, durationValue);

                TextView distanceTextView = activity.findViewById(R.id.distanceRecapTextView);
                distanceTextView.setText(getString(R.string.km, String.valueOf(distance)));

                TextView caloriesTextView = activity.findViewById(R.id.caloriesRecapTextView);
                caloriesTextView.setText(String.valueOf(calories));

                TextView speedTextView = activity.findViewById(R.id.speedRecapTextView);
                speedTextView.setText((getString(R.string.km_h, String.valueOf(avgSpeed))));

                TextView durationTextView = activity.findViewById(R.id.durationTextView);
                durationTextView.setText(duration);

                //Setting floating button listener, on click Db should be updated
                ExtendedFloatingActionButton okButton = activity.findViewById(R.id.fab_ok);
                okButton.setOnClickListener(v -> {

                    progressBar.setVisibility(View.VISIBLE);
                    final int[] runItemId = new int[1];
                    //creating nad running a thread for adding RunItem to the database.
                    Thread addRunItemThread;
                    addRunItemThread = new Thread(() -> {
                        String todayDate = new SimpleDateFormat("yyyy-MM-dd", Locale.ITALY).format(new Date());
                        //adding a run item
                        RunItem runItem = new RunItem(durationValue, distance, avgSpeed, avgPace, calories, todayDate);
                        appRepository.addRunItem(runItem);

                    });
                    addRunItemThread.start();

                    //Creating a thread for adding RunDetailItems.
                    Thread addDetailItemThread;
                    addDetailItemThread = new Thread(() -> {
                        runItemId[0] = appRepository.getLastRunItem().getRun_item_id();
                        for (RunDetailItem data : detailItemList) {
                            data.setRunId(runItemId[0]);
                            appRepository.addRunDetailItem(data);
                        }
                        runDetailItemSingleton.clearItems();
                    });

                    // this DataBaseWorkerChecker is used for check if addRunItemThread has finished. if so
                    // addDetailItemThread can start.
                    new DataBaseWorkerChecker(500, addRunItemThread) {
                        @Override
                        void action() {
                            addDetailItemThread.start();
                        }
                    };

                    // this DataBaseWorkerChecker is used for check if addDetailItemThread has finished. if so
                    //it call checkDataBaseWorkerEnd()
                    new DataBaseWorkerChecker(500, addDetailItemThread) {
                        @Override
                        void action() {
                            RecapFragment.this.checkDataBaseWorkerEnd(addDetailItemThread);
                        }
                    };

                });
            }

        }

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        AppCompatActivity activity = (AppCompatActivity) getActivity();
        if(activity!=null){
            Context context = getContext();
            switch (item.getItemId()) {
                case R.id.profilo:
                    Utility.showAlert(context,activity,UserActivity.class);
                    return false;
                case R.id.attivita:
                    Utility.showAlert(context,activity,ActivitiesActivity.class);
                    return false;
                case R.id.corsa:
                    return true;
                default:
                    return false;
            }
        }
        return false;
    }


    /**
     * Function that calculate average speed during race
     * @return average speed
     */
    private double calculateAvgSpeed(){
        if(detailItemList.size() > 0) {
            double sum = 0;
            for (int i = 0; i < detailItemList.size(); i++) {
                sum += detailItemList.get(i).getSpeed();
            }
            return sum / detailItemList.size();
        }else{
            return 0.0;
        }
    }

    /**
     * This is a recursive function. Checks if thread passed by argument
     * has finished. If not just iterates. Checking process repeats every 500ms.
     * @param thread thread to be checked
     */
    private void checkDataBaseWorkerEnd(final Thread thread){
        Activity activity = getActivity();
        if(activity!= null) {
            Handler handler = new Handler();
            handler.postDelayed(() -> {
                if (!thread.getState().equals(Thread.State.TERMINATED)) {
                    checkDataBaseWorkerEnd(thread);
                } else {
                    progressBar.setVisibility(View.INVISIBLE);

                    Intent goMain = new Intent(activity.getApplication(), MainActivity.class);
                    startActivity(goMain);
                }
            }, 500);
        }
    }
}
