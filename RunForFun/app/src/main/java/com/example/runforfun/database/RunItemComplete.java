package com.example.runforfun.database;

import androidx.room.Embedded;
import androidx.room.Relation;

import java.util.List;

/**
 * Class which represents a single run with all info.
 * So it's a run with a list of detailed items.
 */
public class RunItemComplete {
    @Embedded
    public RunItem runItem;
    @Relation(
            parentColumn = "run_item_id",
            entityColumn = "runId",
            entity = RunDetailItem.class
    )
    public List<RunDetailItem> runDetails;
}
