package com.example.runforfun.utilities;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;

import androidx.core.app.ActivityCompat;

import com.example.runforfun.R;
import com.google.android.material.snackbar.Snackbar;

/**
 * Class for Permission utilities.
 */
public class PermissionUtilities {

    private static final int REQUEST_PERMISSIONS_REQUEST_CODE = 0;
    private static final String LOCATION_PERMISSIONS = Manifest.permission.ACCESS_FINE_LOCATION;

    /**
     * Function that check if the user granted fine location permission.
     * @param activity activity.
     * @return true if granted, false otherwise.
     */
    public static boolean checkPermissions(Activity activity) {
        int permissionState = ActivityCompat.checkSelfPermission(activity, LOCATION_PERMISSIONS);
        return permissionState == PackageManager.PERMISSION_GRANTED;
    }


    /**
     * Function which request location permission to the user.
     * @param activity activity.
     */
    public static void requestLocationPermissions(final Activity activity) {
        boolean shouldProvideRationale =
                ActivityCompat.shouldShowRequestPermissionRationale(activity, LOCATION_PERMISSIONS);

        // Provide an additional rationale to the user if he didn't select the "Don't ask again" option.
        if (shouldProvideRationale) {
            Snackbar.make(
                    activity.findViewById(R.id.fragment_container),
                    R.string.permission_rationale,
                    Snackbar.LENGTH_INDEFINITE)
                    .setAction(R.string.ok, view -> ActivityCompat.requestPermissions(activity,
                            new String[]{LOCATION_PERMISSIONS}, REQUEST_PERMISSIONS_REQUEST_CODE))
                    .show();
        } else {
            ActivityCompat.requestPermissions(activity,
                    new String[]{LOCATION_PERMISSIONS}, REQUEST_PERMISSIONS_REQUEST_CODE);
        }
    }
}
