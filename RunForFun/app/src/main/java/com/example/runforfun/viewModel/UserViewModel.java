package com.example.runforfun.viewModel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.example.runforfun.database.User;
import com.example.runforfun.database.AppRepository;

import java.math.BigDecimal;

/**
 * The ViewModel class is designed to store and manage UI-related data in a lifecycle conscious way.
 * The ViewModel class allows data to survive configuration changes such as screen rotations.
 *
 * The data stored by ViewModel are not for long term. (Until activity is destroyed)
 *
 */
public class UserViewModel extends AndroidViewModel {

    private AppRepository appRepository;
    private LiveData<User> userLiveData;

    public UserViewModel(@NonNull Application application) {
        super(application);
        appRepository = new AppRepository(application);
        userLiveData = appRepository.getUserInfo();
    }

    public LiveData<User> getUserInfo(){
        if (userLiveData == null) {
            userLiveData = appRepository.getUserInfo();
        }
        return userLiveData;
    }

    public String getTotalAvgSpeed(){
        return String.valueOf(BigDecimal.valueOf(appRepository.getTotalAvgSpeed()).setScale(2,BigDecimal.ROUND_UP)); }

    public String getTotalCalories(){
        return String.valueOf(appRepository.getTotalCalories());
    }

    //time received is in millis, so dividing by 60000 -> minutes
    public String getTotalTime(){
        return String.valueOf(BigDecimal.valueOf(Math.floor(appRepository.getTotalTime() / 60000)).longValue());
    }

    public String getTotalDistance(){ return String.valueOf(BigDecimal.valueOf(appRepository.getTotalDistance()).setScale(2,BigDecimal.ROUND_UP));}

    public String getLastMonthTotalCalories(){ return String.valueOf(appRepository.getLastMonthTotalCalories()); }

    public String getLastMonthTotalTime(){ return  String.valueOf(appRepository.getLastMonthTotalTime()/(60*1000));}

    public String getLastMonthTotalDistance(){ return  String.valueOf(BigDecimal.valueOf(appRepository.getLastMonthTotalDistance()).setScale(2,BigDecimal.ROUND_UP));}
}
