package com.example.runforfun;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Chronometer;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.example.runforfun.utilities.LocationService;
import com.example.runforfun.utilities.NotificationService;
import com.example.runforfun.utilities.RunUtilities;
import com.example.runforfun.utilities.Utility;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton;
import com.mapbox.mapboxsdk.Mapbox;
import com.mapbox.mapboxsdk.location.LocationComponent;
import com.mapbox.mapboxsdk.location.LocationComponentActivationOptions;
import com.mapbox.mapboxsdk.location.LocationComponentOptions;
import com.mapbox.mapboxsdk.location.OnCameraTrackingChangedListener;
import com.mapbox.mapboxsdk.location.modes.CameraMode;
import com.mapbox.mapboxsdk.location.modes.RenderMode;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;
import com.mapbox.mapboxsdk.maps.Style;

public class RunFragment extends Fragment implements BottomNavigationView.OnNavigationItemSelectedListener, OnMapReadyCallback, OnCameraTrackingChangedListener {

    private TextView distanceTextView;
    private TextView caloriesTextView;
    private TextView paceTextView;
    private Chronometer chronometer;
    private Intent recapIntent;
    private double distance = 0.0;
    private int calories = 0;
    private MapboxMap mapboxMap;
    private LocationComponent locationComponent;
    private boolean isInTrackingMode;
    private RunUtilities runUtilities = new RunUtilities();
    private boolean isTerminated = false;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        // Mapbox Access token
        Activity activity = getActivity();
        if(activity!=null) {
            Mapbox.getInstance(activity.getApplicationContext(), getString(R.string.mapbox_access_token));
        }
        return inflater.inflate(R.layout.fragment_run, container, false);
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        final AppCompatActivity activity = (AppCompatActivity) getActivity();
        if(activity!=null) {

            //variable used to assign correct value to chronometer in case screen has been rotated.
            SharedPreferences sharedPreferences = activity.getSharedPreferences(Utility.KEY_ACTIVITY_ROTATION, Context.MODE_PRIVATE);
            //prevTime is 0 if there isn't any sharedPreferences and no previous screen rotation.
            long prevTime = sharedPreferences.getLong("time",0);

            chronometer = activity.findViewById(R.id.chronometer);
            chronometer.setFormat("00:%s");
            chronometer.setBase(SystemClock.elapsedRealtime() - prevTime);
            chronometer.start();

            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.remove("time");
            editor.apply();

            Intent serviceIntent = new Intent(activity, LocationService.class);

            if(prevTime == 0) {
                //because of background limitations in android > Oreo we need to use
                //a foreground service instead of background one but this is not suitable for
                //old android versions. So we have to use both Service and ForegroundService
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    activity.startForegroundService(serviceIntent);
                } else {
                    activity.startService(serviceIntent);
                }

                Intent notificationIntent = new Intent(activity, NotificationService.class);
                activity.startService(notificationIntent);
            }

            activity.registerReceiver(broadcastReceiver, new IntentFilter(LocationService.ACTION_LOCATION));

            //Setting Bottom Navigation bar
            BottomNavigationView bottomNavigationView = activity.findViewById(R.id.bottom_nav_bar);
            bottomNavigationView.setOnNavigationItemSelectedListener(this);
            bottomNavigationView.setSelectedItemId(R.id.corsa);


            //Setting floating button listener
            ExtendedFloatingActionButton stopButton = activity.findViewById(R.id.fab_stop);
            stopButton.setOnClickListener(v -> {
                chronometer.stop();
                long durationValue = SystemClock.elapsedRealtime() - chronometer.getBase();
                isTerminated = true;

                recapIntent = new Intent(activity, RecapActivity.class);
                recapIntent.putExtra("duration", chronometer.getText());
                recapIntent.putExtra("durationValue", durationValue);
                recapIntent.putExtra("distance", distance);
                recapIntent.putExtra("calories", calories);
                distance = 0.0;

                //Stopping locationService which is a ForegroundService if Android >= Oreo
                Intent stopLocationService = new Intent(activity, LocationService.class);
                stopLocationService.setAction(Utility.STOP_SERVICE);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    activity.startService(stopLocationService);
                }else { activity.stopService(serviceIntent); }

                //Stopping notification service
                Intent stopNotificationService = new Intent(activity, NotificationService.class);
                activity.stopService(stopNotificationService);

                //Starting new Activity
                activity.startActivity(recapIntent);
            });

            distanceTextView = activity.findViewById(R.id.distanceValueTextView);
            caloriesTextView = activity.findViewById(R.id.caloriesValueTextView);
            paceTextView = activity.findViewById(R.id.rhythmValueTextView);

            MapView mapView = activity.findViewById(R.id.mapView);
            mapView.onCreate(savedInstanceState);
            mapView.getMapAsync(this);

        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        AppCompatActivity activity = (AppCompatActivity) getActivity();
        if(activity != null) {
            return item.getItemId() == R.id.corsa;
        }
        return false;
    }


    /**
     * Broadcast which receive intent from LocationService. It updates also values in GUI.
     */
    private BroadcastReceiver broadcastReceiver =  new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            Bundle extras = intent.getExtras();
            if(extras != null) {
                calories = extras.getInt("calories");
                distance = extras.getDouble("distance");
                double speed = extras.getDouble("speed");
                distanceTextView.setText(String.valueOf(distance));
                caloriesTextView.setText(String.valueOf(calories));
                paceTextView.setText(String.valueOf(runUtilities.calculatePace(speed)));
            }
        }
    };

    private void enableLocationComponent(@NonNull Style loadedMapStyle) {
        Context context = getContext();
        if(context != null) {
            LocationComponentOptions customLocationComponentOptions = LocationComponentOptions.builder(context)
                    .elevation(5)
                    .accuracyAlpha(.6f)
                    .foregroundTintColor(Color.BLUE)
                    .build();

            // Get an instance of the component
            locationComponent = mapboxMap.getLocationComponent();

            LocationComponentActivationOptions locationComponentActivationOptions =
                    LocationComponentActivationOptions.builder(context, loadedMapStyle)
                            .locationComponentOptions(customLocationComponentOptions)
                            .build();

            // Activate with options
            locationComponent.activateLocationComponent(locationComponentActivationOptions);
            locationComponent.setLocationComponentEnabled(true);

            locationComponent.setCameraMode(CameraMode.TRACKING);
            locationComponent.setRenderMode(RenderMode.COMPASS);

            locationComponent.addOnCameraTrackingChangedListener(this);

            Activity activity = getActivity();
            if (activity != null) {
                activity.findViewById(R.id.back_to_camera_tracking_mode).setOnClickListener(view -> {
                    if (!isInTrackingMode) {
                        isInTrackingMode = true;
                        locationComponent.setCameraMode(CameraMode.TRACKING);
                        locationComponent.zoomWhileTracking(16f);
                    }
                });
            }
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Activity activity = getActivity();
        if (activity!= null) {
            activity.unregisterReceiver(broadcastReceiver);
            if (!isTerminated) {
                SharedPreferences.Editor editor = activity.getSharedPreferences(Utility.KEY_ACTIVITY_ROTATION, Context.MODE_PRIVATE).edit();
                editor.putLong("time", SystemClock.elapsedRealtime() - chronometer.getBase());
                editor.apply();
            }
        }
    }

    @Override
    public void onCameraTrackingDismissed() {
        isInTrackingMode = false;
    }

    //Nothing to do here
    @Override
    public void onCameraTrackingChanged(int currentMode) {}

    @Override
    public void onMapReady(@NonNull MapboxMap mapboxMap) {
        this.mapboxMap = mapboxMap;
        mapboxMap.setStyle(Style.LIGHT, this::enableLocationComponent);
    }
}
